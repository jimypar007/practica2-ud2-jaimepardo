package gui;

import base.enums.TipoCoche;
import com.mysql.jdbc.MysqlDataTruncation;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import util.Util;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by Jaime Pardo on 13/12/2021.
 */
public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener, ChangeListener {
    private Modelo modelo;
    private Vista vista;
    boolean refrescar;
    static int mecanicos;
    static int recambios;

    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        mecanicos = 1;
        recambios = 1;
        modelo.conectar(vista);
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    /**
     * Refresca las tablas de todas las vistas
     */
    private void refrescarTodo() {
        refrescarMecanico();
        refrescarCliente();
        refrescarCoche();
        refrescarRecambios();
        refrescar = false;
    }

    /**
     * Añade los listeners a los botones y les asigna un nombre
     * @param listener
     */
    private void addActionListeners(ActionListener listener) {
        vista.btnCocheAnadir.addActionListener(listener);
        vista.btnCocheAnadir.setActionCommand("btnCocheAnadir");
        vista.btnClienteAnadir.addActionListener(listener);
        vista.btnClienteAnadir.setActionCommand("btnClienteAnadir");
        vista.btnMecanicoAnadir.addActionListener(listener);
        vista.btnMecanicoAnadir.setActionCommand("btnMecanicoAnadir");
        vista.btnRecambioAnadir.addActionListener(listener);
        vista.btnRecambioAnadir.setActionCommand("btnRecambioAnadir");
        vista.btnCocheEliminar.addActionListener(listener);
        vista.btnCocheEliminar.setActionCommand("btnCocheEliminar");
        vista.btnClienteEliminar.addActionListener(listener);
        vista.btnClienteEliminar.setActionCommand("btnClienteEliminar");
        vista.btnMecanicoEliminar.addActionListener(listener);
        vista.btnMecanicoEliminar.setActionCommand("btnMecanicoEliminar");
        vista.btnRecambioModificar.addActionListener(listener);
        vista.btnRecambioModificar.setActionCommand("btnRecambioModificar");
        vista.btnCocheModificar.addActionListener(listener);
        vista.btnCocheModificar.setActionCommand("btnCocheModificar");
        vista.btnClienteModificar.addActionListener(listener);
        vista.btnClienteModificar.setActionCommand("btnClienteModificar");
        vista.btnMecanicoModificar.addActionListener(listener);
        vista.btnMecanicoModificar.setActionCommand("btnMecanicoModificar");
        vista.btnEliminarRecambio.addActionListener(listener);
        vista.btnEliminarRecambio.setActionCommand("btnEliminarRecambio");
        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.optionDialog.btnOpcionesGuardar.setActionCommand("guardarOpciones");
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.itemDesconectar.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
        vista.addMecanico.addActionListener(listener);
        vista.addMecanico.setActionCommand("addMecanico");
        vista.addRecambio.addActionListener(listener);
        vista.addRecambio.setActionCommand("addRecambio");
        vista.delMecanico.addActionListener(listener);
        vista.delMecanico.setActionCommand("delMecanico");
        vista.delRecambio.addActionListener(listener);
        vista.delRecambio.setActionCommand("delRecambio");
        vista.combustionRadioButton.addActionListener(listener);
        vista.combustionRadioButton.setActionCommand("Combustion");
        vista.electricoRadioButton.addActionListener(listener);
        vista.electricoRadioButton.setActionCommand("Electrico");
        vista.hibridoRadioButton.addActionListener(listener);
        vista.hibridoRadioButton.setActionCommand("Hibrido");
        vista.tabbedPane.addChangeListener(this);
        vista.mecanicoTabla.getSelectionModel().addListSelectionListener(this);
        vista.recambioTabla.getSelectionModel().addListSelectionListener(this);
        vista.clienteTabla.getSelectionModel().addListSelectionListener(this);
        vista.cochesTabla.getSelectionModel().addListSelectionListener(this);
        vista.cocheRecambioTabla.getSelectionModel().addListSelectionListener(this);
        vista.mecanicoCocheTabla.getSelectionModel().addListSelectionListener(this);
    }

    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    private void addItemListeners(Controlador controlador) {
    }

    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     *
     * @param e Evento producido en una lista
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.mecanicoTabla.getSelectionModel())) {
                int row = vista.mecanicoTabla.getSelectedRow();
                vista.txtNombreMecanico.setText(String.valueOf(vista.mecanicoTabla.getValueAt(row, 1)));
                vista.txtApellidoMecanico.setText(String.valueOf(vista.mecanicoTabla.getValueAt(row, 2)));
                vista.txtTelefonoMecanico.setText(String.valueOf(vista.mecanicoTabla.getValueAt(row, 3)));
            } else if (e.getSource().equals(vista.clienteTabla.getSelectionModel())) {
                int row = vista.clienteTabla.getSelectedRow();
                vista.txtDni.setText(String.valueOf(vista.clienteTabla.getValueAt(row, 1)));
                vista.txtNombre.setText(String.valueOf(vista.clienteTabla.getValueAt(row, 2)));
                vista.txtApellidos.setText(String.valueOf(vista.clienteTabla.getValueAt(row, 3)));
                vista.txtEmail.setText(String.valueOf(vista.clienteTabla.getValueAt(row, 4)));
                vista.txtTelefono.setText(String.valueOf(vista.clienteTabla.getValueAt(row, 5)));
            } else if (e.getSource().equals(vista.cochesTabla.getSelectionModel())) {
                int row = vista.cochesTabla.getSelectedRow();
                asignarTipoCoche(row);
                refrescarRecambioCoche(modelo.idCocheMatricula(String.valueOf(vista.cochesTabla.getValueAt(row, 2))));
                refrescarMecanicoCoche(modelo.idCocheMatricula(String.valueOf(vista.cochesTabla.getValueAt(row, 2))));
                vista.txtMatricula.setText(String.valueOf(vista.cochesTabla.getValueAt(row, 2)));
                vista.comboMarca.setSelectedItem(String.valueOf(vista.cochesTabla.getValueAt(row, 3)));
                vista.fecha.setDate((LocalDate.parse(String.valueOf(vista.cochesTabla.getValueAt(row, 4)))));
                vista.comboCliente.setSelectedItem(String.valueOf(vista.cochesTabla.getValueAt(row, 5)));
                vista.tituloMecanicos.setVisible(true);
                vista.panelMecanicos.setVisible(true);
                vista.panelRecambios.setVisible(true);
                vista.tituloRecambios.setVisible(true);
                vista.agrandarPantalla();
            } else if (e.getSource().equals(vista.recambioTabla.getSelectionModel())) {
                int row = vista.recambioTabla.getSelectedRow();
                vista.txtRecambioNombre.setText(String.valueOf(vista.recambioTabla.getValueAt(row,1)));
                vista.txtRecambioPrecio.setText(String.valueOf(vista.recambioTabla.getValueAt(row,2)));
                if (vista.recambioTabla.getValueAt(row,3).equals("1")){
                    vista.combustionRadioButton1.setSelected(true);
                }else {
                    vista.combustionRadioButton1.setSelected(false);
                }
                if (vista.recambioTabla.getValueAt(row,4).equals("1")){
                    vista.electricoRadioButton1.setSelected(true);
                }else {
                    vista.electricoRadioButton1.setSelected(false);
                }
            }  else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.mecanicoTabla.getSelectionModel())) {
                    borrarCamposMecanico();
                } else if (e.getSource().equals(vista.clienteTabla.getSelectionModel())) {
                    borrarCamposCliente();
                } else if (e.getSource().equals(vista.cochesTabla.getSelectionModel())) {
                    borrarcamposCoche();
                }
            }
        }
    }

    /**
     * Selecciona un radiobutton segun el nombre del parametro
     *
     * @param row Linea en la que se encuentra el coche seleccionado
     */
    private void asignarTipoCoche(int row) {

        if (vista.cochesTabla.getValueAt(row,1).equals("Combustion")){
            vista.combustionRadioButton.setSelected(true);
            refrescarRecambio(true,false);
        }else if (vista.cochesTabla.getValueAt(row,1).equals("Electrico")){
            vista.electricoRadioButton.setSelected(true);
            refrescarRecambio(false,true);
        }else {
            vista.hibridoRadioButton.setSelected(false);
            refrescarRecambio(true,true);
        }

    }

    /**
     * Metodo que asigna las acciones a los botones
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Desconectar":
                modelo.desconectar();
                vista.panel1.setVisible(false);
                break;
            case "Salir":
                System.exit(0);
                break;
            case "abrirOpciones":
                if (String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta");
                }
                break;
            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.tfIP.getText(),
                        vista.optionDialog.tfUser.getText(),
                        String.valueOf(vista.optionDialog.pfPass.getPassword()),
                        String.valueOf(vista.optionDialog.pfAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Vista(),new Modelo());
                break;
            case "btnCocheAnadir":
                try {
                    if (comprobarCocheVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.cochesTabla.clearSelection();
                    } else if (modelo.cocheMatriculaYaExiste(vista.txtMatricula.getText())) {
                        Util.showErrorAlert("Ese coche ya existe");
                        vista.cochesTabla.clearSelection();
                    } else if (modelo.clienteYaAsignado(String.valueOf(vista.comboCliente.getSelectedItem()))) {
                        Util.showErrorAlert("Ese cliente ya tiene un coche");
                        vista.cochesTabla.clearSelection();
                    } else if (camposRepetidosRecambio() || camposRepetidosMecanico()){
                        Util.showErrorAlert("Existen campos repetidos");
                        vista.cochesTabla.clearSelection();
                    } else {
                        modelo.insertarCoche(
                                tipoCoche(),
                                vista.txtMatricula.getText(),
                                String.valueOf(vista.comboMarca.getSelectedItem()),
                                vista.fecha.getDate(),
                                String.valueOf(vista.comboCliente.getSelectedItem()));
                        insertarRecambios(vista.txtMatricula.getText());
                        insertarMecanicos(vista.txtMatricula.getText());
                        borrarcamposCoche();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
                    vista.cochesTabla.clearSelection();
                }
                refrescarCoche();
                break;
            case "btnCocheModificar":
                try {
                    if (comprobarCocheVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.cochesTabla.clearSelection();
                    } else {
                        modelo.modificarCoche(
                                tipoCoche(),
                                vista.txtMatricula.getText(),
                                String.valueOf(vista.comboMarca.getSelectedItem()),
                                vista.fecha.getDate(),
                                String.valueOf(vista.comboCliente.getSelectedItem()),
                                Integer.parseInt((String) vista.cochesTabla.getValueAt(vista.cochesTabla.getSelectedRow(), 0))
                        );

                        modelo.borrarMecanicosCoche(Integer.parseInt((String) vista.cochesTabla.getValueAt(vista.cochesTabla.getSelectedRow(), 0)));
                        modelo.borrarRecambiosCoche(Integer.parseInt((String) vista.cochesTabla.getValueAt(vista.cochesTabla.getSelectedRow(), 0)));
                        insertarRecambios(vista.txtMatricula.getText());
                        insertarMecanicos(vista.txtMatricula.getText());
                        borrarcamposCoche();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
                    vista.cochesTabla.clearSelection();
                }

                refrescarCoche();
                break;
            case "btnCocheEliminar":
                try {
                    modelo.borrarMecanicosCoche(Integer.parseInt((String) vista.cochesTabla.getValueAt(vista.cochesTabla.getSelectedRow(), 0)));
                    modelo.borrarRecambiosCoche(Integer.parseInt((String) vista.cochesTabla.getValueAt(vista.cochesTabla.getSelectedRow(), 0)));
                    modelo.borrarCoche(Integer.parseInt((String) vista.cochesTabla.getValueAt(vista.cochesTabla.getSelectedRow(), 0)));
                    vista.tituloMecanicos.setVisible(false);
                    vista.panelMecanicos.setVisible(false);
                    vista.panelRecambios.setVisible(false);
                    vista.tituloRecambios.setVisible(false);
                    vista.encogerPantalla();
                    borrarcamposCoche();
                    refrescarCoche();
                } catch (ArrayIndexOutOfBoundsException a){
                    Util.showErrorAlert("Selecciona un coche");
                    vista.cochesTabla.clearSelection();
                }

                break;
            case "btnMecanicoAnadir":
                try {
                    if (comprobarMecanicoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.mecanicoTabla.clearSelection();
                    } else {
                            modelo.insertarMecanico(vista.txtNombreMecanico.getText(),
                                    vista.txtApellidoMecanico.getText(),
                                    vista.txtTelefonoMecanico.getText());
                            refrescarMecanico();
                            borrarCamposMecanico();
                    }
                } catch (SQLException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.mecanicoTabla.clearSelection();
                }

                break;
            case "btnMecanicoModificar":
                try {
                    if (comprobarMecanicoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.mecanicoTabla.clearSelection();
                    } else {
                        modelo.modificarMecanico(vista.txtNombreMecanico.getText(),
                                vista.txtApellidoMecanico.getText(),
                                vista.txtTelefonoMecanico.getText(),
                                Integer.parseInt((String) vista.mecanicoTabla.getValueAt(vista.mecanicoTabla.getSelectedRow(), 0)));
                        refrescarMecanico();
                        borrarCamposMecanico();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.mecanicoTabla.clearSelection();
                }
                borrarCamposMecanico();
                break;
            case "btnMecanicoEliminar":
                try {
                    try{
                        modelo.borrarMecanico(Integer.parseInt((String) vista.mecanicoTabla.getValueAt(vista.mecanicoTabla.getSelectedRow(), 0)));
                    }catch (SQLException s){
                        Util.showErrorAlert("Ese mecanico tiene un vehiculo asociado");
                    }

                    borrarCamposMecanico();
                    refrescarMecanico();
                }catch (ArrayIndexOutOfBoundsException a){
                    Util.showErrorAlert("Selecciona un mecanico");
                    vista.cochesTabla.clearSelection();
                }

                break;
            case "btnClienteAnadir":
                try {
                    if (comprobarClienteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.clienteTabla.clearSelection();
                    } else if (modelo.dniClienteYaExiste(vista.txtDni.getText())) {
                        Util.showErrorAlert("Ese DNI ya existe.\nIntroduce un DNI diferente.");
                        vista.clienteTabla.clearSelection();
                    } else {
                        modelo.insertarCliente(vista.txtDni.getText(), vista.txtNombre.getText(),
                                vista.txtApellidos.getText(),
                                vista.txtEmail.getText(),
                                Integer.parseInt(vista.txtTelefono.getText()));
                        refrescarCliente();
                        borrarCamposCliente();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.clienteTabla.clearSelection();
                }

                break;
            case "btnClienteModificar":
                try {
                    if (comprobarClienteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.clienteTabla.clearSelection();
                    } else {
                        modelo.modificarCliente(vista.txtDni.getText(), vista.txtNombre.getText(),
                                vista.txtApellidos.getText(),
                                vista.txtEmail.getText(),
                                vista.txtTelefono.getText(),
                                Integer.parseInt((String) vista.clienteTabla.getValueAt(vista.clienteTabla.getSelectedRow(), 0)));
                        refrescarCliente();
                        borrarCamposCliente();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.clienteTabla.clearSelection();
                }

                break;
            case "btnClienteEliminar":
                try {
                    try {
                        modelo.borrarCliente(Integer.parseInt((String) vista.clienteTabla.getValueAt(vista.clienteTabla.getSelectedRow(), 0)));
                    }catch (SQLException s){
                        Util.showErrorAlert("Ese cliente tiene un vehiculo asociado");
                    }
                }catch (ArrayIndexOutOfBoundsException a){
                    Util.showErrorAlert("Selecciona un cliente");
                    vista.cochesTabla.clearSelection();
                }


                borrarCamposCliente();
                refrescarCliente();
                break;
            case "btnRecambioAnadir":
                try {
                    if (comprobarRecambioVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.recambioTabla.clearSelection();
                    } else {
                        try {
                            modelo.insertarRecambio(vista.txtRecambioNombre.getText(),
                                    vista.txtRecambioPrecio.getText(),
                                    vista.combustionRadioButton1.isSelected(),
                                    vista.electricoRadioButton1.isSelected());
                            refrescarRecambios();
                            borrarCamposRecambio();
                        }catch (SQLException s){
                            Util.showErrorAlert("Valor introducido incorrecto");
                        }

                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.recambioTabla.clearSelection();
                }

                break;
            case "btnRecambioModificar":
                try {
                    if (comprobarRecambioVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.recambioTabla.clearSelection();
                    } else {
                        modelo.modificarRecambio(vista.txtRecambioNombre.getText(),
                                vista.txtRecambioPrecio.getText(),
                                vista.combustionRadioButton1.isSelected(),
                                vista.electricoRadioButton1.isSelected(),
                                Integer.parseInt((String) vista.recambioTabla.getValueAt(vista.recambioTabla.getSelectedRow(), 0)));
                        refrescarRecambios();
                        borrarCamposRecambio();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.recambioTabla.clearSelection();
                } catch (ArrayIndexOutOfBoundsException a){
                    Util.showErrorAlert("Ningun elemento seleccionado");
                    vista.recambioTabla.clearSelection();
                }
                borrarCamposCliente();
                break;
            case "btnEliminarRecambio":
                try {
                    try {
                        modelo.borrarRecambio(Integer.parseInt((String) vista.recambioTabla.getValueAt(vista.recambioTabla.getSelectedRow(), 0)));
                    }catch (SQLException s){
                        Util.showErrorAlert("El recambio esta siendo usado en un coche");
                    }

                    borrarCamposRecambio();
                    refrescarRecambios();
                }catch (ArrayIndexOutOfBoundsException a){
                    Util.showErrorAlert("Selecciona un recambio");
                    vista.cochesTabla.clearSelection();
                }

                break;

            case "addRecambio":
                addRecambio();
                break;
            case "delRecambio":
                delRecambio();
                break;
            case "addMecanico":
                addMecanico();
                break;
            case "delMecanico":
                delMecanico();
                break;
            case "Combustion":
                refrescarRecambio(true,false);
                break;
            case "Electrico":
                refrescarRecambio(false,true);
                break;
            case "Hibrido":
                refrescarRecambio(true,true);
                break;
        }
    }

    /**
     * Inserta los mecanicos de los combobox en el coche
     *
     * @param matricula matricula del coche que se desea añadir los mecanicos
     */
    private void insertarMecanicos(String matricula) {

        for (int i=0;i<mecanicos;i++){
            int id_mecanico = Integer.valueOf(vista.cbMecanico.get(i).getSelectedItem().toString().split(" ")[0]);
            int id_coche = Integer.valueOf(modelo.idCocheMatricula(matricula));
            modelo.asignarMecanicoCoche(id_mecanico,id_coche);
        }

    }

    /**
     * Inserta los recambios de los combobox en el coche
     *
     * @param matricula matricula del coche que se desea añadir los recambios
     */
    private void insertarRecambios(String matricula) {

        for (int i=0;i<recambios;i++){
            int id_recambio = Integer.valueOf(vista.cbRecambio.get(i).getSelectedItem().toString().split(" ")[0]);
            int id_coche = Integer.valueOf(modelo.idCocheMatricula(matricula));
            modelo.asignarRecambioCoche(id_recambio,id_coche);
        }

    }

    /**
     * Devuelve un String con el tipo de coche segun el radiobutton seleccionado
     *
     * @return String tipo de coche
     */
    private String tipoCoche() {

        if (vista.combustionRadioButton.isSelected()){
            return TipoCoche.COMBUSTIBLE.getValor();
        }
        else if (vista.electricoRadioButton.isSelected()){
            return TipoCoche.ELECTRICO.getValor();
        }
        else if (vista.hibridoRadioButton.isSelected()){
            return TipoCoche.HIBRIDO.getValor();
        }

        return "error";

    }

    /**
     * Actualiza los clientes que se ven en la lista y los comboboxes
     */
    private void refrescarCliente() {
        try {
            vista.clienteTabla.setModel(construirTableModelCliente(modelo.consultarCliente()));
            vista.comboCliente.removeAllItems();
            for(int i = 0; i < vista.dtmClientes.getRowCount(); i++) {
                vista.comboCliente.addItem(vista.dtmClientes.getValueAt(i, 0)+" - "+
                        vista.dtmClientes.getValueAt(i, 2)+" "+ vista.dtmClientes.getValueAt(i, 3));
            }
        } catch (SQLException e) {
        } catch (NullPointerException n){
        }
    }

    /**
     * Crea el DTM de mecanicos
     *
     * @param rs datos de la tabla
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelMecanico(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmMecanicos.setDataVector(data, columnNames);
        return vista.dtmMecanicos;
    }

    /**
     * Actualiza los mecanicos que se ven en la lista y los comboboxes
     */
    private void refrescarMecanico() {
        try {
            vista.mecanicoTabla.setModel(construirTableModelMecanico(modelo.consultarMecanico()));

           for (int i = 0; i < vista.cbMecanico.size();i++ ){
               vista.cbMecanico.get(i).removeAllItems();
               for(int x = 0; x < vista.dtmMecanicos.getRowCount(); x++) {
                   vista.cbMecanico.get(i).addItem(vista.dtmMecanicos.getValueAt(x, 0)+" - "+
                           vista.dtmMecanicos.getValueAt(x, 2)+", "+vista.dtmMecanicos.getValueAt(x, 1));
               }
           }

        } catch (SQLException e) {
        } catch (NullPointerException n){
        }
    }

    /**
     * Crea DTM de clientes
     *
     * @param rs datos de la tabla
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelCliente(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmClientes.setDataVector(data, columnNames);
        return vista.dtmClientes;
    }

    /**
     * Actualiza los coches que se ven en la lista y los comboboxes
     */
    private void refrescarCoche() {
        try {
            vista.cochesTabla.setModel(construirTableModelCoche(modelo.consultarCoche()));
        } catch (SQLException e) {
        } catch (NullPointerException n){
        }
    }

    /**
     * Crea el DTM de Coches
     *
     * @param rs datos de la tabla
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelCoche(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmCoches.setDataVector(data, columnNames);
        return vista.dtmCoches;

    }

    /**
     * Actualiza los recambios que se ven en la lista y los comboboxes
     */
    private void refrescarRecambios() {

        try {
            vista.recambioTabla.setModel(construirTableModelRecambio(modelo.consultarRecambio()));
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException n){
        }

    }

    /**
     * Actualiza los recambios que se ven en la lista y los comboboxes
     */
    private void refrescarRecambio(boolean combustion, boolean electrico) {

        try {

            if (combustion && electrico){
                vista.recambioTabla.setModel(construirTableModelRecambio(modelo.consultarRecambioHibrido()));
            }else if (combustion){
                vista.recambioTabla.setModel(construirTableModelRecambio(modelo.consultarRecambioCombustion()));
            }else if (electrico){
                vista.recambioTabla.setModel(construirTableModelRecambio(modelo.consultarRecambioElectrico()));
            };

            for (int i = 0; i < vista.cbRecambio.size();i++ ){
                vista.cbRecambio.get(i).removeAllItems();
                for(int x = 0; x < vista.dtmRecambios.getRowCount(); x++) {
                    vista.cbRecambio.get(i).addItem(vista.dtmRecambios.getValueAt(x, 0)+" - "+
                            vista.dtmRecambios.getValueAt(x, 1)+", "+vista.dtmRecambios.getValueAt(x, 2)+"€");
                }
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Crea el DTM de recambios
     *
     * @param rs datos de la tabla
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelRecambio(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmRecambios.setDataVector(data, columnNames);
        return vista.dtmRecambios;
    }

    /**
     * Refresca los recambio de un coche
     *
     * @param coche id del coche
     */
    private void refrescarRecambioCoche(int coche) {

        try {
            vista.cocheRecambioTabla.setModel(construirTableModelRecambioCoche(modelo.consultarRecambioCoche(coche)));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Crea el DTM de recambios de un coche
     *
     * @param rs datos de la tabla
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelRecambioCoche(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmRecambioCoche.setDataVector(data, columnNames);
        return vista.dtmRecambioCoche;
    }

    /**
     * Refresca la tabla de mecanicos de un coche
     *
     * @param coche
     */
    private void refrescarMecanicoCoche(int coche) {

        try {
            vista.mecanicoCocheTabla.setModel(construirTableModelMecanicoCoche(modelo.consultarMecanicoCoche(coche)));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Crea el DTM de los mecanicos de un coche
     *
     * @param rs datos de la table
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelMecanicoCoche(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmMecanicoCoche.setDataVector(data, columnNames);
        return vista.dtmMecanicoCoche;
    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    private void setOptions() {
     }

    /**
     * Borra todos los campos del coche
     */
    private void borrarcamposCoche() {
        vista.comboCliente.setSelectedIndex(-1);
        for (int i = 0;i<vista.cbMecanico.size();i++){
            vista.cbMecanico.get(i).setSelectedIndex(-1);
        }
        for (int i = 0;i<vista.cbRecambio.size();i++){
            vista.cbRecambio.get(i).setSelectedIndex(-1);
        }
        vista.txtMatricula.setText("");
        vista.fecha.setText("");
    }

    /**
     * Borra todos los campos del cliente
     */
    private void borrarCamposCliente() {
        vista.txtNombre.setText("");
        vista.txtDni.setText("");
        vista.txtApellidos.setText("");
        vista.txtEmail.setText("");
        vista.txtTelefono.setText("");
    }

    /**
     * Borrar todos los campos del mecanico
     */
    private void borrarCamposMecanico() {
        vista.txtNombreMecanico.setText("");
        vista.txtApellidoMecanico.setText("");
        vista.txtTelefonoMecanico.setText("");
    }

    /**
     * Borrar todos los campos de recambio
     */
    private void borrarCamposRecambio() {
        vista.txtRecambioPrecio.setText("");
        vista.txtRecambioNombre.setText("");
        vista.combustionRadioButton1.setSelected(false);
        vista.electricoRadioButton1.setSelected(false);
    }

    /**
     * Comprueba si los datos de coche estan vacios
     *
     * @return
     */
    private boolean comprobarCocheVacio() {

        boolean cbMecanicoVacio = true;
        for (int i = 0;i<mecanicos;i++){
            cbMecanicoVacio = vista.cbMecanico.get(i).getSelectedIndex() == -1;
        }
        boolean cbRecambioVacio = true;
        for (int i = 0;i<recambios;i++){
            cbRecambioVacio = vista.cbRecambio.get(i).getSelectedIndex() == -1;
        }

        return vista.txtMatricula.getText().isEmpty() ||
                cbMecanicoVacio ||
                cbRecambioVacio ||
                vista.comboCliente.getSelectedIndex() == -1 ||
                vista.fecha.getText().isEmpty();
    }

    /**
     * Comprueba si los datos de cliente estan vacios
     *
     * @return
     */
    private boolean comprobarClienteVacio() {
        return vista.txtDni.getText().isEmpty() ||
                vista.txtNombre.getText().isEmpty() ||
                vista.txtApellidos.getText().isEmpty() ||
                vista.txtTelefono.getText().isEmpty() ||
                vista.txtEmail.getText().isEmpty();
    }

    /**
     * Comprueba si los datos de mecanico estan vacios
     *
     * @return
     */
    private boolean comprobarMecanicoVacio() {
        return vista.txtNombreMecanico.getText().isEmpty() ||
                vista.txtApellidoMecanico.getText().isEmpty() ||
                vista.txtTelefonoMecanico.getText().isEmpty();
    }

    /**
     * Comprueba si los datos de recambio estan vacios
     *
     * @return boolean de si esta vacio o no
     */
    private boolean comprobarRecambioVacio() {

        if (!vista.combustionRadioButton1.isSelected() && !vista.electricoRadioButton1.isSelected()){
            return true;
        }

        if (vista.txtRecambioNombre.getText().isEmpty() || vista.txtRecambioPrecio.getText().isEmpty()){
            return true;
        }

        return false;

    }

    /**
     * Comprueba si los datos de mecanicos estan vacios
     *
     * @return boolean de si esta vacio o no
     */
    private boolean camposRepetidosMecanico() {

        ArrayList<Integer> ids = new ArrayList<>();

        for (int i = 0;i<mecanicos;i++){
            ids.add(Integer.valueOf(vista.cbMecanico.get(i).getSelectedItem().toString().split(" ")[0]));
        }

        for (int i = 1; i<ids.size();i++){
            if (ids.get(i) == ids.get(i-1))
                return true;
        }

        return false;

    }

    /**
     * Comprueba si los datos de recambio estan vacios
     *
     * @return boolean de si esta vacio o no
     */
    private boolean camposRepetidosRecambio() {

        ArrayList<Integer> ids = new ArrayList<>();

        for (int i = 0;i<recambios;i++){
            ids.add(Integer.valueOf(vista.cbRecambio.get(i).getSelectedItem().toString().split(" ")[0]));
        }

        for (int i = 1; i<ids.size();i++){
            if (ids.get(i) == ids.get(i-1))
                return true;
        }

        return false;

    }


    /**
     * Borra un CB de mecanico en el coche
     */
    private void delMecanico() {

        for (int i=vista.cbMecanico.size()-1;i>=0;i--){

            if (vista.cbMecanico.get(i).isVisible() && i!=0){
                vista.cbMecanico.get(i).setVisible(false);
                vista.cbMecanico.get(i).setSelectedIndex(-1);
                vista.cbtMecanico.get(i).setVisible(false);
                mecanicos--;
                return;
            }
        }
    }

    /**
     * Añade un CB de mecanico en el coche
     */
    private void addMecanico() {

        for (int i=0;i<vista.cbMecanico.size();i++){

            if (!vista.cbMecanico.get(i).isVisible() && i!=0){
                vista.cbMecanico.get(i).setVisible(true);
                vista.cbtMecanico.get(i).setVisible(true);
                mecanicos++;
                return;
            }
        }

    }

    /**
     * Borra un CB de recambio en el coche
     */
    private void delRecambio() {

        for (int i=vista.cbRecambio.size()-1;i>=0;i--){

            if (vista.cbRecambio.get(i).isVisible() && i!=0){
                vista.cbRecambio.get(i).setVisible(false);
                vista.cbRecambio.get(i).setSelectedIndex(-1);
                vista.cbtRecambio.get(i).setVisible(false);
                recambios--;
                return;
            }
        }

    }

    /**
     * Añade un CB de recambio en el coche
     */
    private void addRecambio() {

        for (int i=0;i<vista.cbRecambio.size();i++){

            if (!vista.cbRecambio.get(i).isVisible() && i!=0){
                vista.cbRecambio.get(i).setVisible(true);
                vista.cbtRecambio.get(i).setVisible(true);
                recambios++;
                return;
            }
        }

    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    /**
     * Metodo que se ejecuta cada vez que se cambia de pestaña
     *
     * @param e
     */
    @Override
    public void stateChanged(ChangeEvent e) {
        refrescarRecambios();
        vista.tituloMecanicos.setVisible(false);
        vista.panelMecanicos.setVisible(false);
        vista.panelRecambios.setVisible(false);
        vista.tituloRecambios.setVisible(false);
        vista.encogerPantalla();
        refrescarCoche();
    }
}
