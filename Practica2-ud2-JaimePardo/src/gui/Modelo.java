package gui;

import com.mysql.jdbc.MysqlDataTruncation;
import com.mysql.jdbc.exceptions.MySQLDataException;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;
import com.sun.corba.se.impl.orb.PrefixParserAction;
import com.sun.xml.internal.bind.v2.TODO;
import util.Util;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Properties;

/**
 * Created by Jaime Pardo on 13/12/2021.
 */
public class Modelo {
    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    public String getIp() {
        return ip;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public Modelo() {
        getPropValues();
    }

    private Connection conexion;

    /**
     * Metodo que conecta con la base de datos
     *
     * @param vista
     */
    void conectar(Vista vista) {
            try(Connection con = DriverManager.getConnection("jdbc:mysql://"+ip+":3307", user, password);
                Statement stmt = con.createStatement();
            ) {
                String sql = "CREATE DATABASE if not EXISTS tallerJaimePardo";
                stmt.executeUpdate(sql);

                try {

                    conexion = DriverManager.getConnection("jdbc:mysql://"+ip+":3307/tallerJaimePardo", user, password);
                    PreparedStatement statement = null;

                    String code = leerFichero();
                    String[] query = code.split("--");
                    for (String aQuery : query) {
                        statement = conexion.prepareStatement(aQuery);
                        statement.executeUpdate();
                    }
                    assert statement != null;
                    statement.close();
                } catch (SQLException | IOException e1) {
                }


            } catch (SQLException sql) {
                vista.panel1.setVisible(false);
                Util.showErrorAlert("No se ha podido conectar con la base");
            }


        }

    /**
     * Metodo que lee el fichero SQL
     *
     * @return
     * @throws IOException
     */
    private String leerFichero() throws IOException {
        //basedatos_java no tiene delimitador
        //StringBuilder es dinamica
        try (BufferedReader reader = new BufferedReader(new FileReader("basedatos_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }
            return stringBuilder.toString();
        }
    }

    /**
     * Metodo que desconecta la base
     */
    void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException e) {
        }  catch (NullPointerException n){
        }
    }

    /**
     * Metodo que inserta Cliente
     */
    void insertarCliente(String dni, String nombre, String apellidos, String email, int telefono) {
        String sentenciaSql = "INSERT INTO cliente (dni, nombre, apellidos, email, telefono)" +
                "VALUES (?,?,?,?,?)";

        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, dni);
            sentencia.setString(2, nombre);
            sentencia.setString(3, apellidos);
            sentencia.setString(4, email);
            sentencia.setInt(5, telefono);
            sentencia.executeUpdate();
        } catch (SQLException e) {
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * Metodo que inserta Mecanico
     */
    void insertarMecanico(String nombre, String apellidos, String telefono) throws SQLException{
        String sentenciaSql = "INSERT INTO mecanico(nombre, apellidos, telefono) VALUES (?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setString(3, telefono);
            sentencia.executeUpdate();
        }finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo que inserta Coche
     */
    void insertarCoche(String tipo, String matricula, String marca, LocalDate fechaAlta, String cliente) {
        String sentenciaSql = "INSERT INTO coche (tipo, matricula, marca, fecha_alta, id_cliente) " +
                "VALUES (?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        int idCliente = Integer.valueOf(cliente.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, tipo);
            sentencia.setString(2, matricula);
            sentencia.setString(3, marca);
            sentencia.setDate(4, Date.valueOf(fechaAlta));
            sentencia.setInt(5, idCliente);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo que relaciona un recambio con un coche
     */
    void asignarRecambioCoche(int id_recambio, int id_coche) {
        String sentenciaSql = "INSERT INTO recambio_coche (id_coche,id_recambio) " +
                "VALUES (?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id_coche);
            sentencia.setInt(2, id_recambio);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo que relaciona un mecanico con un coche
     */
    public void asignarMecanicoCoche(int id_mecanico, int id_coche) {

        String sentenciaSql = "INSERT INTO mecanico_coche (id_coche,id_mecanico) " +
                "VALUES (?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id_coche);
            sentencia.setInt(2, id_mecanico);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }

    }

    /**
     * Metodo que inserta un recambio
     */
    public void insertarRecambio(String nombre, String precio, boolean comb, boolean elec) throws SQLException {
        String sentenciaSql = "INSERT INTO `tallerjaimepardo`.`recambio` (`componente`, `precio`, `combustion`, `electrico`) VALUES (?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, precio);
            sentencia.setBoolean(3, comb);
            sentencia.setBoolean(4, elec);
            sentencia.executeUpdate();
        }finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo que modifica un cliente
     */
    void modificarCliente(String dni, String nombre, String apellidos, String email, String telefono, int idCliente) {
        String sentenciaSql = "UPDATE cliente SET dni=?,nombre=?, apellidos=?, email=?, telefono=?" +
                "WHERE id=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, dni);
            sentencia.setString(2, nombre);
            sentencia.setString(3, apellidos);
            sentencia.setString(4, email);
            sentencia.setString(5, telefono);
            sentencia.setInt(6, idCliente);
            sentencia.executeUpdate();
        } catch (SQLException e) {
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo que modifica un mecanico
     */
    void modificarMecanico(String nombre, String apellidos, String telefono, int idMecanico) {

        String sentenciaSql = "UPDATE mecanico SET nombre = ?, apellidos = ?, telefono = ?" +
                "WHERE id = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setString(3, telefono);
            sentencia.setInt(4, idMecanico);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo que modifica un coche
     */
    void modificarCoche(String tipo, String matricula, String marca, LocalDate fechaAlta, String cliente, int idCoche) {

        String sentenciaSql = "UPDATE coche SET tipo = ?, matricula = ?, marca = ?, fecha_alta = ?, id_cliente = ? WHERE id = ?";
        PreparedStatement sentencia = null;

        int idCliente = Integer.valueOf(cliente.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, tipo);
            sentencia.setString(2, matricula);
            sentencia.setString(3, marca);
            sentencia.setDate(4, Date.valueOf(fechaAlta));
            sentencia.setInt(5, idCliente);
            sentencia.setInt(6, idCoche);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo que modifica un recambio
     */
    public void modificarRecambio(String nombre, String precio, boolean comb, boolean elec, int idRecambio) {

        String sentenciaSql = "UPDATE recambio SET componente = ?, precio = ?, combustion = ?, electrico= ? WHERE id = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, precio);
            sentencia.setBoolean(3, comb);
            sentencia.setBoolean(4, elec);
            sentencia.setInt(5, idRecambio);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }

    }

    /**
     * Metodo que borra un mecanico
     */
    void borrarMecanico(int idMecanico) throws SQLException{
        String sentenciaSql = "DELETE FROM mecanico WHERE id=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idMecanico);
            sentencia.executeUpdate();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo que borra un cliente
     */
    void borrarCliente(int idCliente) throws SQLException {
        String sentenciaSql = "DELETE FROM cliente WHERE id = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idCliente);
            sentencia.executeUpdate();
        }finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo que borra un coche
     */
    void borrarCoche(int idCoche) {
        String sentenciaSql = "DELETE FROM coche WHERE id = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idCoche);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo que borra un recambio
     */
    void borrarRecambio(int idRecambio) throws SQLException {
        String sentenciaSql = "DELETE FROM recambio WHERE id=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idRecambio);
            sentencia.executeUpdate();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Consulta los campos de mecanicos
     *
     * @return ResulSet de los valores obtenidos
     * @throws SQLException
     */
    ResultSet consultarMecanico() throws SQLException, NullPointerException {

        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(nombre) AS 'Nombre mecanico', " +
                "concat(apellidos) AS 'Apellidos', concat(telefono) AS 'Teléfono' FROM mecanico";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Consulta los campos de cliente
     *
     * @return ResulSet de los valores obtenidos
     * @throws SQLException
     */
    ResultSet consultarCliente() throws SQLException, NullPointerException  {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(dni) AS 'DNI', concat(nombre) AS 'Nombre', concat(apellidos) AS 'Apellidos', " +
                "concat(email) AS 'Email', concat(telefono) AS 'Teléfono' FROM cliente";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Consulta los campos de coche
     *
     * @return ResulSet de los valores obtenidos
     * @throws SQLException
     */
    ResultSet consultarCoche() throws SQLException, NullPointerException  {
        String sentenciaSql = "SELECT concat(co.id) AS 'ID', concat(co.tipo) AS 'Tipo', concat(co.matricula) AS 'Matricula', " +
                "concat(co.marca) AS 'Marca', " +
                "concat(co.fecha_alta) AS 'Alta', " +
                "concat(cl.id, ' - ', cl.apellidos, ', ', cl.nombre) AS 'Cliente' FROM coche AS co " +
                "INNER JOIN cliente AS cl ON cl.id = co.id_cliente";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Consulta los campos de mecanicos de un coche
     *
     * @return ResulSet de los valores obtenidos
     * @throws SQLException
     */
    ResultSet consultarMecanicoCoche(int idCoche) throws SQLException, NullPointerException  {
        String sentenciaSql = "SELECT concat(mecanico.nombre) as 'Nombre',concat(mecanico.apellidos) as 'Apellidos',concat(mecanico.telefono) as 'Telefono' FROM mecanico INNER JOIN mecanico_coche ON mecanico.id=mecanico_coche.id_mecanico " +
                "INNER JOIN coche ON coche.id=mecanico_coche.id_coche WHERE coche.id=?";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setInt(1,idCoche);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Consulta los campos de recambio de un coche
     *
     * @return ResulSet de los valores obtenidos
     * @throws SQLException
     */
    ResultSet consultarRecambioCoche(int idCoche) throws SQLException, NullPointerException  {
        String sentenciaSql = "SELECT concat(recambio.id) as 'ID',concat(recambio.componente) as 'Componente',concat(recambio.precio) as 'Precio' FROM recambio " +
                "INNER JOIN recambio_coche ON recambio.id=recambio_coche.id_recambio " +
                "INNER JOIN coche ON coche.id=recambio_coche.id_coche WHERE coche.id=?";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setInt(1,idCoche);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Consulta los campos de recambios
     *
     * @return ResulSet de los valores obtenidos
     * @throws SQLException
     */
    ResultSet consultarRecambio() throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(componente) AS 'Componente', " +
                "concat(precio) AS 'Precio', concat(combustion) AS 'Combustion', concat(electrico) AS 'Electrico' FROM recambio";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Consulta los campos de recambios de combustion
     *
     * @return ResulSet de los valores obtenidos
     * @throws SQLException
     */
    ResultSet consultarRecambioCombustion() throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(componente) AS 'Componente', " +
                "concat(precio) AS 'Precio', concat(combustion) AS 'Combustion', concat(electrico) AS 'Electrico' " +
                "FROM recambio WHERE combustion=true";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Consulta los campos de recambios electricos
     *
     * @return ResulSet de los valores obtenidos
     * @throws SQLException
     */
    ResultSet consultarRecambioElectrico() throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(componente) AS 'Componente', " +
                "concat(precio) AS 'Precio', concat(combustion) AS 'Combustion', concat(electrico) AS 'Electrico' " +
                "FROM recambio WHERE electrico=true";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Consulta los campos de recambios hibridos
     *
     * @return ResulSet de los valores obtenidos
     * @throws SQLException
     */
    ResultSet consultarRecambioHibrido() throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(componente) AS 'Componente', " +
                "concat(precio) AS 'Precio', concat(combustion) AS 'Combustion', concat(electrico) AS 'Electrico' " +
                "FROM recambio WHERE combustion=true OR electrico=true";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }


    /**
     * Usa los datos del cuadro de dialogo
     *
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Asigna los datos del cuadro de dialogo a properties
     *
     */
    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.ip=ip;
        this.user=user;
        this.password=pass;
        this.adminPassword=adminPass;
    }

    /**
     * Comprueba si la matricula ya existe
     *
     * @param matricula matricula a comprobar
     * @return
     */
    public boolean cocheMatriculaYaExiste(String matricula) {
        String consulta="SELECT existeMatricula(?)";
        PreparedStatement function;
        boolean matExists=false;
        try {
            function=conexion.prepareStatement(consulta);
            function.setString(1,matricula);
            ResultSet rs =function.executeQuery();
            rs.next();
            matExists=rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return matExists;
    }

    /**
     * Comrpueba que el dni del cliente ya existe
     *
     * @param dni DNI a comprobar
     * @return
     */
    public boolean dniClienteYaExiste(String dni) {
        String dniConsulta = "SELECT existeDniCliente(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(dniConsulta);
            function.setString(1, dni);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }

    /**
     * Comprueba si el cliente ya tiene un coche asignado
     *
     * @param cliente Cliente a comprobar
     * @return
     */
    public boolean clienteYaAsignado(String cliente) {

        int idCliente = Integer.valueOf(cliente.split(" ")[0]);

        String clienteConsulta = "SELECT clienteAsignado(?)";
        PreparedStatement function;
        boolean clienteAsignado = false;
        try {
            function = conexion.prepareStatement(clienteConsulta);
            function.setInt(1, idCliente);
            ResultSet rs = function.executeQuery();
            rs.next();

            clienteAsignado = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clienteAsignado;

    }

    /**
     * Devuelve el id de un coche dada su matricula
     *
     * @param matricula Matricula del coche a buscar
     * @return
     */
    public int idCocheMatricula(String matricula) {

        int id = 0;

        try {

            String sentenciaSql = "SELECT id FROM coche WHERE matricula=?";
            PreparedStatement sentencia = null;

            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, matricula);
            ResultSet rs = sentencia.executeQuery();
            rs.next();

            id = rs.getInt(1);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return id;
    }

    /**
     * Borra los mecanicos de un coche
     *
     * @param idCoche ID del coche a buscar
     */
    public void borrarMecanicosCoche(int idCoche) {

        String sentenciaSql = "DELETE FROM mecanico_coche WHERE id_coche = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idCoche);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }

    }

    /**
     * Borra los recambio de un coche
     *
     * @param idCoche ID del coche a buscar
     */
    public void borrarRecambiosCoche(int idCoche) {

        String sentenciaSql = "DELETE FROM recambio_coche WHERE id_coche = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idCoche);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }

    }
}
