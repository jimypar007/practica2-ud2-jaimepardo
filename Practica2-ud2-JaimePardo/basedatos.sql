REATE DATABASE tallerJaimePardo;
--
USE tallerJaimePardo;
--
create table if not exists cliente (
id int auto_increment primary key,
dni VARCHAR(30) not null,
nombre VARCHAR(30) not null,
apellidos VARCHAR(30) not null,
email VARCHAR(30) not null,
telefono int
);
--
create table if not exists coche (
id int auto_increment primary key,
tipo VARCHAR(20) not null,
matricula VARCHAR(20) not null UNIQUE,
marca VARCHAR(40) not null,
fecha_alta DATE,
id_cliente int,
FOREIGN KEY (id_cliente) REFERENCES cliente(id)
);
--
create table if not exists mecanico(
id int auto_increment primary key,
nombre VARCHAR(30) not null,
apellidos VARCHAR(30) not null,
telefono int
);
--
create table if not exists mecanico_coche(
id int AUTO_INCREMENT PRIMARY KEY,
id_coche int,
id_mecanico int,
FOREIGN KEY (id_coche) REFERENCES coche(id),
FOREIGN KEY (id_mecanico) REFERENCES mecanico(id)
);
--
create table if not exists recambio(
id int auto_increment primary key,
componente varchar(30),
precio float,
combustion boolean,
electrico boolean
);
--
create table if not exists recambio_coche(
id int AUTO_INCREMENT PRIMARY KEY,
id_coche int,
id_recambio int,
FOREIGN KEY (id_coche) REFERENCES coche(id),
FOREIGN KEY (id_recambio) REFERENCES recambio(id)
);
--
delimiter ||
create function existeMatricula(f_matricula varchar(40))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(id) from coche)) do
    if  ((select matricula from coche where id = (i + 1)) like f_matricula) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function if not exists  existeDniCliente(f_dni varchar(50))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(id) from cliente)) do
    if  ((select dni from cliente where id = (i + 1)) like f_dni) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function if not exists clienteAsignado(f_id varchar(50))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(id) from coche)) do
    if  ((select id_cliente from coche where id = (i + 1)) like f_id) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;
--
INSERT IGNORE INTO `tallerjaimepardo`.`recambio` (`id`, `componente`, `precio`, `combustion`, `electrico`) VALUES ('1', 'Ruedas', '100', '1', '1');
INSERT IGNORE INTO `tallerjaimepardo`.`recambio` (`id`, `componente`, `precio`, `combustion`, `electrico`) VALUES ('2', 'Motor', '700', '1', '0');
INSERT IGNORE INTO `tallerjaimepardo`.`recambio` (`id`, `componente`, `precio`, `combustion`, `electrico`) VALUES ('3', 'Motor electrico', '1500', '0', '1');
INSERT IGNORE INTO `tallerjaimepardo`.`recambio` (`id`, `componente`, `precio`, `combustion`, `electrico`) VALUES ('4', 'Puertas', '200', '1', '1');
INSERT IGNORE INTO `tallerjaimepardo`.`recambio` (`id`, `componente`, `precio`, `combustion`, `electrico`) VALUES ('5', 'Escape', '150', '1', '0');
INSERT IGNORE INTO `tallerjaimepardo`.`recambio` (`id`, `componente`, `precio`, `combustion`, `electrico`) VALUES ('6', 'Bujias', '60', '1', '0');
INSERT IGNORE INTO `tallerjaimepardo`.`recambio` (`id`, `componente`, `precio`, `combustion`, `electrico`) VALUES ('7', 'Aceite', '20', '1', '0');
INSERT IGNORE INTO `tallerjaimepardo`.`recambio` (`id`, `componente`, `precio`, `combustion`, `electrico`) VALUES ('8', 'Parachoques', '50', '1', '1');